from django.shortcuts import render

def index(request):
    vars = {"test": "EDem works!"}
    return render(request,'default/index.html',context=vars)