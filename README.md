#  EDem

![Логотип не загрузился](https://gitlab.com/grandcore/gc-edem/-/raw/master/README.files/edem-logo.png)

Движок для проектов нацеленных на совместную работу и принятие решений демократическим спомособ. На нём будет работать как платформа фонда [GrandCore](https://gitlab.com/grandcore/grandcore-mvp), так и ряд наших проектов, например, открытая образовательная платформа [GrandCoreScool](https://gitlab.com/grandcore/gc-school) 

